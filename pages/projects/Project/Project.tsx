import { IProject } from '../types';
import './Project.css';

interface Props {
    project: IProject;
}

const Project: React.FC<Props> = ({ project }: Props) => {
    const { link, image, sourceLink, text, title } = project;

    return (
        <div className="project-wrapper">
            <a href={link} target="_blank" rel="noreferrer" className="project-link">
                <div className="project">
                    <div className="project-image">
                        <img src={image} />
                    </div>

                    <div className="project-body">
                        <h2 className="project-title">{title}</h2>
                        <div className="project-text">{text}</div>
                    </div>
                </div>
            </a>
            <a href={sourceLink} className="project-link source" target="_blank" rel="noreferrer">
                Source
            </a>
        </div>
    );
};

export default Project;
