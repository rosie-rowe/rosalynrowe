export interface IProject {
    image: string;

    link: string;
    sourceLink: string;

    title: string;

    /** body text */
    text: string;
}
