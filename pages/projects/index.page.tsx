import List from '../../renderer/List/List';
import Project from './Project/Project';
import { IProject } from './types';

export { Page };

const projects: IProject[] = [
    {
        link: 'https://krossr.com',
        image: 'assets/krossr.png',
        sourceLink: 'https://gitlab.com/krossr/krossr-client',
        title: 'Krossr',
        text: 'Mobile-friendly Picross app with user-generated content',
    },
    {
        link: 'https://www.nexusmods.com/subnautica/mods/1305',
        image: 'assets/snmod.png',
        sourceLink: 'https://gitlab.com/rosie-rowe/subnautica-mods',
        title: 'Subnautica - Quick Unpin Recipes',
        text: 'A mod for Subnautica that allows right clicks to unpin recipes',
    },
    {
        link: 'https://wordsearch.rosalynrowe.com',
        image: 'assets/wordsearch.png',
        sourceLink: 'https://gitlab.com/rosie-rowe/wordsearch',
        title: 'Word Search Generator',
        text: 'Word Search Generator that can create a playable version or an image',
    },
];

function Page() {
    return (
        <>
            <List items={projects} renderItem={(project) => <Project project={project} />} />
        </>
    );
}
