import { PropsWithChildren } from 'react';
import './Content.css';

const Content: React.FC<PropsWithChildren> = ({ children }: PropsWithChildren) => {
    return (
      <div className='main-content'>
        {children}
      </div>
    )
}

export default Content;
