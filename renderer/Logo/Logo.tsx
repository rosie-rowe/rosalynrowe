import logo from './logo.svg';
import './Logo.css';

const Logo: React.FC = () =>{
    return (
      <div className='logo'>
        <a href="/">
          <img src={logo} height={64} width={64} alt="logo" />
        </a>
      </div>
    );
}

export default Logo;
