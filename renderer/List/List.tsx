import React from 'react';

interface Props<TItem> {
    items: TItem[];
    renderItem: (item: TItem) => React.ReactElement;
}

const List: <TItem>(props: Props<TItem>) => React.ReactElement = ({ items, renderItem }) => {
    return <>{items.map((item) => renderItem(item))}</>;
};

export default List;
