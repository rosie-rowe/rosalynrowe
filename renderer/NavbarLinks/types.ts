interface INavbarLink {
    /** URL */
    href: string;

    /** Display text */
    text: string;
}

export { INavbarLink };
