import { Link } from '../Link';
import { INavbarLink } from './types';

interface Props {
    link: INavbarLink;
}

/** Renders a single navbar link */
const NavbarLink: React.FC<Props> = ({ link }: Props) => {
    const { href, text } = link;

    return (
        <Link className="navitem" href={href}>
            {text}
        </Link>
    );
};

export default NavbarLink;
