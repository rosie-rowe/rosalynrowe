import List from '../List/List';
import NavbarLink from './NavbarLink';
import { INavbarLink } from './types';

const links: INavbarLink[] = [
    { href: '/', text: 'Home' },
    { href: '/about', text: 'About' },
    { href: '/projects', text: 'Projects' },
];

/** Renders all navbar links for the app */
const NavbarLinks: React.FC = () => {
    return (
        <>
            <List items={links} renderItem={(link) => <NavbarLink key={link.href} link={link} />} />
        </>
    );
};

export default NavbarLinks;
