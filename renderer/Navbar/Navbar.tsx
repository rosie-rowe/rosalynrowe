import { Link } from '../Link';
import Logo from '../Logo/Logo';
import NavbarLinks from '../NavbarLinks/NavbarLinks';
import './Navbar.css';

const Navbar: React.FC = () => {
    return (
        <div className="navbar">
            <Logo />
            <NavbarLinks />
        </div>
    );
};

export default Navbar;
