import React, { PropsWithChildren } from 'react'
import { PageContextProvider } from './usePageContext'
import type { PageContext } from './types'
import './PageShell.css'
import Layout from './Layout/Layout'

interface Props extends PropsWithChildren {
  pageContext: PageContext;
}

const PageShell: React.FC<Props> = ({ children, pageContext }: Props) => {
  return (
    <React.StrictMode>
      <PageContextProvider pageContext={pageContext}>
        <Layout>
          {children}
        </Layout>
      </PageContextProvider>
    </React.StrictMode>
  )
}

export default PageShell;
