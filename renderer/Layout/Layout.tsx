import { PropsWithChildren } from 'react';
import Content from '../Content/Content';
import Navbar from '../Navbar/Navbar';
import './Layout.css';

const Layout: React.FC<PropsWithChildren> = ({ children }: PropsWithChildren) => {
    return (
      <div className='layout'>
        <Navbar />
        <Content>{children}</Content>
      </div>
    )
  }

export default Layout;
